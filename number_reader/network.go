package main

import (
	mat "gonum.org/v1/gonum/mat"
	"math/rand"
	"math"
)

type Network struct {
	NumLayers int
	sizes []int //contains the size of each layer
	biases []mat.Matrix //biases for each layer
	weights []mat.Matrix //weights for each layer
}

func CreateNetwork(n int, s []int) *Network{
	b:=make([]mat.Matrix,0)
		for key, value := range s {
			if key == 0 {
				b = append(b,nil)
			} else {
				temp := make([]float64,0)
				for i:=0; i < value; i ++ {
					temp = append(temp, rand.Float64())
				}
				b = append(b,mat.NewDense(s[key],1,temp))
			}

		}
	w := make ([]mat.Matrix,0)
	for key, value := range s {
		temp := make([]float64,0)
		for i:=0; i < value; i ++ {
			temp = append(temp, rand.Float64())
		}
		b = append(b,mat.NewDense(s[key],1,temp))

	}

	return &Network{
		n,
		s,
		b,
		w,
	}
}

func Sigmoid (z float64) float64{
	return 1.0/(1.0+math.Exp(-1*z))
}

type Neuron interface {

}
